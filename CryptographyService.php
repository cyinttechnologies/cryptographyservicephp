<?php

namespace CYINT\ComponentsPHP\Services;

class CryptographyService
{

    private $key;
    private $key_size;
    private $iv;
    private $iv_size; 
    private $algorithm;
    private $mode; 
    private $encoded;

    function __construct( $key = null, $salt = null, $algorithm = MCRYPT_RIJNDAEL_128, $mode = MCRYPT_MODE_CBC )
    {  
        if(empty($key))
            throw new \Exception('Missing private encryption key to encrypt content', 500);

        $this->algorithm = $algorithm;
        $this->mode      = $mode; 
        $this->key       = pack('H*',$key);
        $this->key_size  = strlen( $key ); 
        $this->iv = empty($salt) ? null : $salt;
    }


    public function getIVSize()
    {
        $iv_size = empty( $this->iv_size ) ? mcrypt_get_iv_size( $this->algorithm, $this->mode ) : $this->iv_size;        
        return $iv_size;
    }


    public function encrypt( $plaintext, $provided_salt = false, $encode = true )
    {
        if(empty($plaintext)) return '';

        $this->iv_size = $this->getIVSize(); 

        if( $provided_salt === false && empty($this->iv) )
        {
            $this->iv = mcrypt_create_iv( $this->iv_size, MCRYPT_RAND );
        }
        else
        {
            $this->iv = empty($provided_salt) ? $this->iv : $provided_salt; 
        }

        $ciphertext = $this->iv . mcrypt_encrypt( $this->algorithm, $this->key, $plaintext, $this->mode, $this->iv );

        if( $encode )
            $ciphertext = base64_encode( $ciphertext );

        return $ciphertext;
    }


    public function getSalt( $ciphertext, $decode = true )
    {
        $cipher_with_iv = $decode ? base64_decode( $ciphertext ) : $ciphertext;                
        $iv             = substr( $cipher_with_iv, 0, $this->iv_size );
        
        return $iv;
    }


    public function decrypt( $ciphertext, $salt_in_object = false, $decode = true )
    {
        if(empty($ciphertext)) return '';
     
        $cipher_with_iv = $decode ? base64_decode( $ciphertext ) : $ciphertext;        
        $this->iv_size  = $this->getIVSize();

        $iv             = $salt_in_object === false ? substr( $cipher_with_iv, 0, $this->iv_size ) : $this->iv;
        $cipher         = substr(  $cipher_with_iv, $this->iv_size, strlen( $cipher_with_iv )  );

        $plaintext      = mcrypt_decrypt( $this->algorithm, $this->key, $cipher, $this->mode, $iv );

        $plaintext = str_replace("\0",'',$plaintext);
        return $plaintext;
    }

}

?>
