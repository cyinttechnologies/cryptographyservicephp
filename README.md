# CryptographyService

A Symfony service for providing cryptographic functions.

Be sure to add service reference to your services.yml

    app.cryptography:
        class: CYINT\ComponentsPHP\Services\CryptographyService
        arguments: ['%app.cryptography.encryption_key%', '%app.cryptography.encryption_salt%'] 

where %app.cryptography.encryption_key is the key and app.cryptography.encryption_salt is the salt.

